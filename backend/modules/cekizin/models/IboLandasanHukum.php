<?php

namespace backend\modules\cekizin\models;

use Yii;

/**
 * This is the model class for table "ibo_landasan_hukum".
 *
 * @property int $id_ibo_landasan_hukum
 * @property string $nama_landasan_hukum
 * @property string $nama_singkatan
 * @property string $published
 * @property string $retribusi
 * @property string $upload_file
 * @property string|null $created_at
 * @property string|null $updated_at
 * @property int|null $created_by
 * @property int|null $updated_by
 * @property int|null $active
 */
class IboLandasanHukum extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ibo_landasan_hukum';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nama_landasan_hukum', 'nama_singkatan', 'published', 'retribusi', 'upload_file'], 'required'],
            [['created_at', 'updated_at'], 'safe'],
            [['created_by', 'updated_by', 'active'], 'integer'],
            [['nama_landasan_hukum', 'nama_singkatan'], 'string', 'max' => 255],
            [['published'], 'string', 'max' => 110],
            [['retribusi'], 'string', 'max' => 100],
            [['upload_file'], 'string', 'max' => 500],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_ibo_landasan_hukum' => 'Id Ibo Landasan Hukum',
            'nama_landasan_hukum' => 'Nama Landasan Hukum',
            'nama_singkatan' => 'Nama Singkatan',
            'published' => 'Published',
            'retribusi' => 'Retribusi',
            'upload_file' => 'Upload File',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
            'active' => 'Active',
        ];
    }
}
