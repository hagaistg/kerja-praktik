<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\cekizin\models\IboLandasanHukumSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Landasan Hukum';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ibo-landasan-hukum-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Ibo Landasan Hukum', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id_ibo_landasan_hukum',
            'nama_landasan_hukum',
            'nama_singkatan',
            'published',
            'retribusi',
            //'upload_file',
            //'created_at',
            //'updated_at',
            //'created_by',
            //'updated_by',
            //'active',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
