<?php

namespace backend\modules\satuansyrt;

/**
 * satuansyrt module definition class
 */
class satuansyrt extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'backend\modules\satuansyrt\controllers';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
