<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\modules\perizinan\models\IboMasterPengaduanOnline */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="ibo-master-pengaduan-online-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'id_master_jenis_pengaduan')->textInput() ?>

    <?= $form->field($model, 'nama')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'no_hp')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'alamat')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'deskripsi_pengaduan')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'konfirmasi_kasie')->textInput() ?>

    <?= $form->field($model, 'detail_kasie')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'konfirmasi_kabid')->textInput() ?>

    <?= $form->field($model, 'detail_kabid')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'konfirmasi_sekretaris')->textInput() ?>

    <?= $form->field($model, 'detail_sekretaris')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'konfirmasi_kadis')->textInput() ?>

    <?= $form->field($model, 'detail_kadis')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'proses_selesai')->textInput() ?>

    <?= $form->field($model, 'kesimpulan')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'file_pendukung')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'alasan_kasie')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'alasan_kabid')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'alasan_sekretaris')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'created_at')->textInput() ?>

    <?= $form->field($model, 'updated_at')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
