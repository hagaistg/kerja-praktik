<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\IboMasterPengaduanOnlineSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = ' Pengaduan Online';

?>
<style type="text/css">
    body {
  background: url('https://i.ytimg.com/vi/NiZh-3aEmSo/maxresdefault.jpg') no-repeat center center fixed;
  -webkit-background-size: cover;
  -moz-background-size: cover;
  background-size: cover;
  -o-background-size: cover;
}
    .CustomClass {
  color: #000000;
}
  </style>
<div class="ibo-master-pengaduan-online-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Buat  Pengaduan Online', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'options' => [ 

            'class' => 'CustomClass',
            'style' => 1==1 ? 'background-color:#00FFFF':'background-color:#0000FF'
                        ],
        
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            // 'id',
            // 'id_master_jenis_pengaduan',
            'nama',
            'no_hp',
            'email:email',
            'alamat',
            'deskripsi_pengaduan',
            //'konfirmasi_kasie',
            //'detail_kasie',
            //'konfirmasi_kabid',
            //'detail_kabid',
            //'konfirmasi_sekretaris',
            //'detail_sekretaris',
            //'konfirmasi_kadis',
            //'detail_kadis',
            //'proses_selesai',
            //'kesimpulan',
            //'file_pendukung',
            //'alasan_kasie',
            //'alasan_kabid',
            //'alasan_sekretaris',
            //'created_at',
            //'updated_at',

            
        ],
    ]); ?>


</div>
